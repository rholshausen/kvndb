use std::str::FromStr;

use log::{LevelFilter, SetLoggerError};
use simplelog::{ColorChoice, Config, TerminalMode, TermLogger};

pub fn setup_loggers(level: &str) -> Result<(), SetLoggerError> {
  let log_level = match level {
    "none" => LevelFilter::Off,
    _ => LevelFilter::from_str(level).unwrap()
  };
  TermLogger::init(log_level, Config::default(), TerminalMode::Stderr, ColorChoice::Auto)
}
