use std::env;
use std::fs::File;
use std::io::{BufReader, stdin, stdout};

use ansi_term::Colour::*;
use anyhow::anyhow;
use bson::Document;
use clap::{App, AppSettings, Arg, ArgMatches, ErrorKind};
use serde_json::Value;

use kvndb_lib::dbfile::write_db_file;
use kvndb_utils::setup_loggers;

fn setup_app<'a, 'b>(program: &str, version: &'b str) -> App<'a, 'b> {
  App::new(program)
    .version(version)
    .about("kvndb - utility to create a DB file from JSON")
    .version_short("v")
    .arg(Arg::with_name("loglevel")
      .short("l")
      .long("loglevel")
      .takes_value(true)
      .use_delimiter(false)
      .possible_values(&["error", "warn", "info", "debug", "trace", "none"])
      .help("Log level (defaults to warn)"))
    .arg(Arg::with_name("INPUT")
      .help("JSON file to pack as a DB file")
      .required(true)
      .index(1))
}

fn handle_matches(args: &ArgMatches) -> Result<(), i32> {
  let log_level = args.value_of("loglevel");
  if let Err(err) = setup_loggers(log_level.unwrap_or("warn")) {
    eprintln!("WARN: Could not setup loggers: {}", err);
    eprintln!();
  }

  let input = args.value_of("INPUT").unwrap();
  if !input.is_empty() {
    match read_json(input) {
      Ok(json) => {
        match bson::to_bson(&json) {
          Ok(data) => {
            let mut doc = Document::new();
            doc.insert("data".to_string(), data);
            match write_db_file(&mut stdout(), doc) {
              Ok(_) => Ok(()),
              Err(err) => {
                eprintln!("{}{}\n", Red.paint("ERROR - Failed to write DB file - "),
                          Red.paint(err.to_string()));
                Err(3)
              }
            }
          },
          Err(err) => {
            eprintln!("{}{}{}{}\n", Red.paint("ERROR - Failed to convert JSON to BSON '"),
              Red.paint(input),
              Red.paint("' - "),
              Red.paint(err.to_string()));
            Err(2)
          }
        }
      }
      Err(err) => {
        eprintln!("{}{}{}{}\n", Red.paint("ERROR - Failed to read input '"),
          Red.paint(input),
          Red.paint("' - "),
          Red.paint(err.to_string()));
        Err(1)
      }
    }
  } else {
    eprintln!("{}\n", Red.paint("ERROR - Input file name is empty"));
    Err(1)
  }
}

fn read_json(path: &str) -> anyhow::Result<Value> {
  if path == "-" {
    serde_json::from_reader(stdin())
  } else {
    let file = File::open(path)?;
    serde_json::from_reader(BufReader::new(file))
  }.map_err(|err| anyhow!("JSON error: {}", err))
}

fn handle_cli() -> Result<(), i32> {
  let args: Vec<String> = env::args().collect();
  let program = args[0].clone();
  let app = setup_app(&program, clap::crate_version!());
  let matches = app
    .setting(AppSettings::ArgRequiredElseHelp)
    .setting(AppSettings::ColoredHelp)
    .get_matches_safe();

  match matches {
    Ok(results) => handle_matches(&results),
    Err(ref err) => {
      match err.kind {
        ErrorKind::HelpDisplayed => {
          println!("{}", err.message);
          Ok(())
        },
        ErrorKind::VersionDisplayed => Ok(()),
        _ => err.exit()
      }
    }
  }
}

fn main() {
  match handle_cli() {
    Ok(_) => (),
    Err(err) => std::process::exit(err)
  }
}
