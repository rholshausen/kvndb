use std::io::Write;
use bson::Document;

/// Write the BSON data to the writer
pub fn write_db_file(out: &mut dyn Write, data: Document) -> anyhow::Result<()> {
  write_header(out)?;
  data.to_writer(out)?;
  Ok(())
}

fn write_header(out: &mut dyn Write) -> anyhow::Result<()> {
  out.write("KVNDB".as_bytes())?;
  out.write(&[0x01, 0x00])?;
  Ok(())
}
